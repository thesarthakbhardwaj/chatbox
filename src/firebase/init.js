import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyCI9W7Y1w4dQNEL57zBSw5EeFvhxNBvf48",
    authDomain: "chatbox-6f88e.firebaseapp.com",
    databaseURL: "https://chatbox-6f88e.firebaseio.com",
    projectId: "chatbox-6f88e",
    storageBucket: "chatbox-6f88e.appspot.com",
    messagingSenderId: "22400645071",
    appId: "1:22400645071:web:33bedd6cc041bfaddcc4ba",
    measurementId: "G-6LC1L8YDLC"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebaseApp.firestore().settings({timestampsInSnapshots : true})
  //export firebase database
  export default firebaseApp.firestore()